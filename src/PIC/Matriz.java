
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import com.googlecode.javacv.cpp.opencv_core.CvScalar;
 import com.googlecode.javacv.cpp.opencv_core.IplImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvGet2D;
 
 /**
  *
  * @author Gianfranco
  */
 public class Matriz {
 
     private CvScalar s3;
     private Integer mat[][];
     private IplImage img;
     private Integer height, width;
 
     public Matriz(IplImage img) {
         this.img = img;
         s3 = null;
         height = img.height();
         width = img.width();
         mat = new Integer[height][width];
         CriaMatriz();
     }
 
     private void CriaMatriz() {
 
         for (int i = 0; i < height; i++) {
             for (int j = 0; j < width; j++) {
                 s3 = cvGet2D(img, i, j);
                 mat[i][j] = (int)s3.val(0);
 
             }
         }
     }
 
     public Integer[][] getMat() {
         return mat;
     }
     
     
 
 }

