
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import static com.googlecode.javacv.cpp.opencv_core.CV_8UC1;
 import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
 import com.googlecode.javacv.cpp.opencv_core.IplImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
 import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvSplit;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
 import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
 
 /**
  *
  * @author Gianfranco
  */
 public class BuscaImagem {
 
     private final IplImage imagem;
     private final InfoImagem im;
     private final Integer npercentil;
     private final Integer npartes;
     private final String diretorio;
     private short op =  1, ophist;
     private Histograma h;
     private IplImage[] partes;
     private IplImage img;
 
     public BuscaImagem(IplImage imagem, String cabecalho) {
         this.imagem = imagem;
         npartes = Integer.parseInt(cabecalho.split(" ")[2]);
         npercentil = Integer.parseInt(cabecalho.split(" ")[3]);
         im = new InfoImagem(npartes, npercentil, "imagem");
         diretorio = cabecalho.split(" ")[9];
 
         partes = new IplImage[npartes];
 
         if (cabecalho.split(" ")[4].equals("1"))ophist=1;
         else if (cabecalho.split(" ")[5].equals("1")) ophist=2;
         else if (cabecalho.split(" ")[6].equals("1"))ophist = 3;
         else if (cabecalho.split(" ")[7].equals("1"))ophist=4;
         
         if(cabecalho.split(" ")[4].equals("0") || cabecalho.split(" ")[5].equals("0") || cabecalho.split(" ")[6].equals("0") || cabecalho.split(" ")[7].equals("0"))
             op=2;
 
             
         
         this.calcula();
 
     }
 
     private void calcula() {
 
         Histograma[] hlbp = new Histograma[4];
         Percentil[] p = new Percentil[4];
         IplImage r, g, b, c;
         if (op == 1) {
             partes = CarregaBaseImagens.DivideImagem(imagem, (int) Math.sqrt(npartes));
             h = new Histograma(partes);
             for (int y = 0; y < npartes; y++) {
                 r = cvCreateImage(cvGetSize(partes[y]), IPL_DEPTH_8U, CV_8UC1);
                 g = cvCreateImage(cvGetSize(partes[y]), IPL_DEPTH_8U, CV_8UC1);
                 b = cvCreateImage(cvGetSize(partes[y]), IPL_DEPTH_8U, CV_8UC1);
                 c = cvCreateImage(cvGetSize(partes[y]), IPL_DEPTH_8U, CV_8UC1);
 
                 cvCvtColor(partes[y], c, CV_BGR2GRAY);
                 cvSplit(partes[y], b, g, r, null);
 
                 p[0] = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][0]);
 
                 hlbp[0] = new Histograma(new LBP(new Matriz(r).getMat()).getM());
                 im.setHistrogramas(y, 0, h.getHistograma_norm_rgb_partes()[y][0], p[0].getPercentil(), hlbp[0].getHistograma_norm());
 
                 p[1] = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][1]);
                 hlbp[1] = new Histograma(new LBP(new Matriz(g).getMat()).getM());
                 im.setHistrogramas(y, 1, h.getHistograma_norm_rgb_partes()[y][1], p[1].getPercentil(), hlbp[1].getHistograma_norm());
 
                 p[2] = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][2]);
                 hlbp[2] = new Histograma(new LBP(new Matriz(b).getMat()).getM());
                 im.setHistrogramas(y, 2, h.getHistograma_norm_rgb_partes()[y][2], p[2].getPercentil(), hlbp[2].getHistograma_norm());
 
                 p[3] = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][3]);
                 hlbp[3] = new Histograma(new LBP(new Matriz(c).getMat()).getM());
                 im.setHistrogramas(y, 3, h.getHistograma_norm_rgb_partes()[y][3], p[3].getPercentil(), hlbp[3].getHistograma_norm());
 
                 cvReleaseImage(r);
                 cvReleaseImage(g);
                 cvReleaseImage(b);
                 cvReleaseImage(c);
 
             }
         }
 
         if (op == 2) {
             switch (ophist) {
                 case 1: //R
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
                     cvSplit(imagem, null, null, img, null);
 
                     break;
                 case 2: //G
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
                     cvSplit(imagem, null, img, null, null);
 
                     break;
                 case 3: //B
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
                     cvSplit(imagem, img, null, null, null);
 
                     break;
                 case 4: // ESCALA DE CINZA
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, 1);
                     cvCvtColor(imagem, img, CV_BGR2GRAY);
 
                     break;
                 default: // ESCALA DE CINZA
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, 1);
                     cvCvtColor(imagem, img, CV_BGR2GRAY);
 
                     break;
             }
 
             partes = CarregaBaseImagens.DivideImagem(img, (int) Math.sqrt(npartes));
             cvReleaseImage(img);
             Histograma histogramalbp;
             Histograma hist;
             Percentil percentil;
             int aux = 0;
 
             for (int d = 0; d < npartes; d++) {
                 Matriz m = new Matriz(partes[d]);
                 hist = new Histograma(m.getMat());
                 percentil = new Percentil(npercentil, hist.getHistograma_norm());
                 histogramalbp = new Histograma(new LBP(m.getMat()).getM());
                 im.setHistrogramas(d, ophist-1, hist.getHistograma_norm(), percentil.getPercentil(), histogramalbp.getHistograma_norm());
                 
                 cvReleaseImage(partes[d]);
             }
             hist = null;
         }
 
     }
 
     public InfoImagem getIm() {
         return im;
     }
 
 }

