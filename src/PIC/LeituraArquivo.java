
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
 import static com.googlecode.javacv.cpp.opencv_highgui.cvShowImage;
 import static com.googlecode.javacv.cpp.opencv_highgui.cvWaitKey;
 import java.io.BufferedReader;
 import java.io.FileReader;
 import java.io.IOException;
 import java.util.ArrayList;
 import java.util.Collections;
 import java.util.Comparator;
 import java.util.List;
 
 /**
  *
  * @author Gianfranco
  */
 public class LeituraArquivo {
 
     private Integer nimagens, npartes, npercentis;
     private boolean hr, hg, hb, hc, hlbp, flagcabecalho = false, flagnome = false, flagpartes = false,flagfim=false;
     private String nomeimagem, h, histp, histlbp, cabecalho,diretorio;
     private InfoImagem im = null;
     private Integer cont = 0;
     private Integer contlinha = 0;
     private List<ND> lista = new ArrayList<>();
     private BuscaImagem bi;
 
     
     public LeituraArquivo(String nome) {
         this.histlbp = "";
         this.h = "";
         this.histp = "";
         try {
             FileReader arq = new FileReader(nome);
             BufferedReader lerArq = new BufferedReader(arq);
 
             String linha = lerArq.readLine();
             contlinha++;
             String[] partes = null;
 
             while (linha != null) {
 
                 partes = linha.split(" ");
 
                 if (flagpartes && !flagfim) {
                     if (partes[0].equals("PARTE")) {
                         linha = lerArq.readLine();
                         contlinha++;
                         partes = linha.split(" ");
                         int aux = 0, op =  1;
 
                         while (linha != null && !partes[0].equals("PARTE") && !partes[0].equals("-a") && !partes[0].equals("-f")) {
                             for (int i = 1; i < partes.length; i++) {
                                 h = h.concat(partes[i]);
                                 h = h.concat(" ");
                             }
                             switch (partes[0]) {
                                 case "R":
                                     hr = true;
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histp = histp.concat(partes[i]);
                                             histp = histp.concat(" ");
                                         }
                                     }
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histlbp = histlbp.concat(partes[i]);
                                             histlbp = histlbp.concat(" ");
                                         }
                                     }
                                     break;
                                 case "G":
 
                                     hg = true;
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histp = histp.concat(partes[i]);
                                             histp = histp.concat(" ");
                                         }
                                     }
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histlbp = histlbp.concat(partes[i]);
                                             histlbp = histlbp.concat(" ");
                                         }
                                     }
                                     break;
                                 case "B":
 
                                     hb = true;
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histp = histp.concat(partes[i]);
                                             histp = histp.concat(" ");
                                         }
                                     }
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histlbp = histlbp.concat(partes[i]);
                                             histlbp = histlbp.concat(" ");
                                         }
                                     }
                                     break;
                                 case "CINZA":
 
                                     hc = true;
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histp = histp.concat(partes[i]);
                                             histp = histp.concat(" ");
                                         }
                                     }
                                     linha = lerArq.readLine();
                                     contlinha++;
                                     if (linha != null) {
                                         partes = linha.split(" ");
                                         for (int i = 1; i < partes.length; i++) {
                                             histlbp = histlbp.concat(partes[i]);
                                             histlbp = histlbp.concat(" ");
                                         }
                                     }
                                     break;
                             }
                             if (hr) {
                                 op = 0;
                             }
                             if (hg) {
                                 op = 1;
                             }
                             if (hb) {
                                 op = 2;
                             }
                             if (hc) {
                                 op = 3;
                             }
                             if(op!= 1)im.setHistrogramas(aux, op, InfoImagem.stringToDouble(h), InfoImagem.stringToDouble(histp), InfoImagem.stringToDouble(histlbp));
                             h = "";
                             histp = "";
                             histlbp = "";
                             hr = false;
                             hg = false;
                             hb = false;
                             hc = false;
                             op =  1;
                             linha = lerArq.readLine();contlinha++;                            
                             if(linha != null)partes = linha.split(" ");
                             
 
                             if (partes[0].equals("PARTE")) {
                                 aux++;
                                 linha = lerArq.readLine();contlinha++;
                                 partes = linha.split(" ");
                             }
                         }
 
                     }                                                                                          /* 0:Perc&LBP; 1:Percentil ; 2: LBP*/
                     lista.add(new ND(diretorio+"//"+nomeimagem,InfoImagem.diferenca(im.getHistrogramas(), bi.getIm().getHistrogramas(),0)));
                     flagpartes = false;
                     flagnome = true;                    
                     cont++;
                 }
 
                 if (flagnome && !flagfim) {                    
                     if(partes[0].equals("-f"))
                          flagfim=true;
                     else{
                         im=null;
                         nomeimagem=linha.split(" ")[1];
                         im= new InfoImagem(npartes, npercentis, nomeimagem);
                         flagpartes = true;
                         flagnome = false;
                     }
                     
                 }
 
                 // ler cabecalho
                 if (!flagcabecalho && partes[0].equals("-c")&& !flagfim) {
                     cabecalho=linha;
                     nimagens = Integer.parseInt(partes[1]);
                     npartes = Integer.parseInt(partes[2]);
                     npercentis = Integer.parseInt(partes[3]);  
                     diretorio = cabecalho.split(" ")[9];                    
                     flagcabecalho = true;
                     flagnome = true;
                     bi = new BuscaImagem(cvLoadImage("image.orig//950.jpg"),getCabecalho());
 
                 }
                 if(flagfim){
                     
                     Collections.sort(lista, new Comparator<ND> () {
 
                         @Override
                         public int compare(ND o1, ND o2) {
                             return o1.getDistancia().compareTo(o2.getDistancia());
                             
                         }
                     });
 
                     for(int i = 0;i<lista.size() && i<10 ;i++){                        
                         cvShowImage(Integer.toString(i+1),cvLoadImage(lista.get(i).getNome()));
                         System.out.println(lista.get(i).getNome()+" - "+lista.get(i).getDistancia());
                     }
                     cvWaitKey();
                     break;
                     
                     
                 }
                 
                 linha = lerArq.readLine();
                 contlinha++; // lÃª da segunda atÃ© a Ãºltima linha
             }
 
             arq.close();
         } catch (IOException e) {
             System.err.printf("Erro na abertura do arquivo: %s.\n",
                     e.getMessage());
         }
 
         System.out.println();
     }
 
     public String getCabecalho() {
         return cabecalho;
     }
 
     
 }
 
 

