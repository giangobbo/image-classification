
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import static com.googlecode.javacv.cpp.opencv_core.CV_8UC1;
 import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
 import com.googlecode.javacv.cpp.opencv_core.IplImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
 import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvSplit;
 import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
 import static com.googlecode.javacv.cpp.opencv_highgui.cvShowImage;
 import static com.googlecode.javacv.cpp.opencv_highgui.cvWaitKey;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2HSV;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2YCrCb;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2YUV;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_THRESH_BINARY;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_THRESH_OTSU;
 import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
 import static com.googlecode.javacv.cpp.opencv_imgproc.cvDilate;
 import static com.googlecode.javacv.cpp.opencv_imgproc.cvErode;
 import static com.googlecode.javacv.cpp.opencv_imgproc.cvThreshold;
 import java.math.BigDecimal;
 import java.math.RoundingMode;
 
 /**
  *
  * @author Gianfranco
  */
 public class MatrizPixel extends Thread {
 
     /**
      * @param args the command line arguments
      */
     @SuppressWarnings("empty statement")
     public  MatrizPixel (String imagem ) {
         // TODO code application logic here
         
         int altura,comprimento;
         startThread st = new startThread();
         Thread tr=new Thread(st),tg=new Thread(st),tb=new Thread(st),th=new Thread(st),ts=new Thread(st),tv=new Thread(st),tcinza=new Thread(st),tY=new Thread(st),tCb=new Thread(st),tCr=new Thread(st);
         
         
        
         
         IplImage img = cvLoadImage(imagem);
         
 //        // conversao para hsv e escala de cinza
 //        IplImage hsvimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);
         IplImage cinzaimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,1);        
 //        IplImage YCrCbimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);        
 //        IplImage YUVimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);        
 //        cvCvtColor(img,hsvimg,CV_BGR2HSV);
         cvCvtColor(img,cinzaimg,CV_BGR2GRAY);
 //        cvCvtColor(img,YCrCbimg,CV_BGR2YCrCb);
 //        cvCvtColor(img,YUVimg,CV_BGR2YUV);
 //        
 //        // separaÃ§Ã£o dos canais R G B         
 //        IplImage r = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
 //        IplImage g = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
 //        IplImage b = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
 //        cvSplit( img, b, g, r, null );       
 //
 //        // separaÃ§Ã£o dos canais H S V
 //        IplImage hue = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
 //        IplImage sat = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
 //        IplImage val = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);        
 //        cvSplit( hsvimg, hue, sat, val, null );
 //        
 //        // separaÃ§Ã£o dos canais Y Cr Cb
 //        IplImage y = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
 //        IplImage cr = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
 //        IplImage cb = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);        
 //        cvSplit( YCrCbimg,y, cr, cb, null );
         
         // pegar valor de cada pixel R G B
 //        CvScalar s;
 //        Double matb[][] = new Double[b.height()][b.width()];
 //        Double matg[][] = new Double[g.height()][g.width()];
 //        Double matr[][] = new Double[r.height()][r.width()];
 //        for(int i=0;i<img.height();i++)
 //            for(int j=0;j<img.width();j++){
 //                s=cvGet2D(img,i,j);
 //                matb[i][j]=s.val(0);
 //                matg[i][j]=s.val(1);
 //                matr[i][j]=s.val(2);
 //            }
 //        System.out.println("R ");
 //        new ImprimeMatriz(matr);        
 //        System.out.println("G ");
 //        new ImprimeMatriz(matg);
 //        System.out.println("B ");
 //        new ImprimeMatriz(matb);
 //        
         
         // pegar valor de cada pixel H S V
 //        CvScalar s2 = null;
 //        double aux;
 //        Double mathue[][] = new Double[hue.height()][hue.width()];
 //        Double matsat[][] = new Double[sat.height()][sat.width()];
 //        Double matval[][] = new Double[val.height()][val.width()];
 //        for(int i=0;i<hsvimg.height();i++)
 //            for(int j=0;j<hsvimg.width();j++){
 //                s2=cvGet2D(hsvimg,i,j);
 //                mathue[i][j]=s2.val(0)/1.8;
 //                matsat[i][j]=round(s2.val(1)/2.56,0);
 //                matval[i][j]=round(s2.val(2)/2.56,0);
 //            }
 //        System.out.println("\nh ");
 //        new ImprimeMatriz((mathue);        
 //        System.out.println("s ");
 //        new ImprimeMatriz((matsat);
 //        System.out.println("v ");
 //        new ImprimeMatriz((matval);
         
         
         // pegar valor de cada pixel escala de cinza
         
 //////////        CvScalar s3 = null;
 //////////        Double matcinza[][] = new Double[cinzaimg.height()][cinzaimg.width()];
 //////////        
 //////////        for(int i=0;i<cinzaimg.height();i++)
 //////////            for(int j=0;j<cinzaimg.width();j++){
 //////////                s3=cvGet2D(cinzaimg,i,j);
 //////////                matcinza[i][j]=s3.val(0);
 //////////                
 //////////            }
         
         
         
 //        System.out.println("\ncinza ");
 //        new ImprimeMatriz((matcinza);  
         
          // pegar valor de cada pixel Y Cr Cb
 //        CvScalar s4;
 //        Double maty[][] = new Double[y.height()][y.width()];
 //        Double matcr[][] = new Double[cr.height()][cr.width()];
 //        Double matcb[][] = new Double[cb.height()][cb.width()];
 //        for(int i=0;i<YCrCbimg.height();i++)
 //            for(int j=0;j<YCrCbimg.width();j++){
 //                s4=cvGet2D(img,i,j);
 //                maty[i][j]=s4.val(1);
 //                matcr[i][j]=s4.val(2);
 //                matcb[i][j]=s4.val(0);
 //            }
 //        System.out.println("Y ");
 //        new ImprimeMatriz((maty);        
 //        System.out.println("Cr ");
 //        new ImprimeMatriz((matcr);
 //        System.out.println("Cb ");
 //        new ImprimeMatriz((matcb);
         
 
 //        Double maty2[][] = new Double[y.height()][y.width()];
 //        Double matcr2[][] = new Double[cr.height()][cr.width()];
 //        Double matcb2[][] = new Double[cb.height()][cb.width()];
 //        Double vet[];
 //        for(int i=0;i<YCrCbimg.height();i++)
 //            for(int j=0;j<YCrCbimg.width();j++){
 //                s4=cvGet2D(img,i,j);
 //                vet = converter_RGB_para_YCbCr(s4.val(2),s4.val(1), s4.val(0));
 //                maty2[i][j]=vet[0];
 //                matcr2[i][j]=vet[1];
 //                matcb2[i][j]=vet[2];
 //            }
 //        System.out.println("Y ");
 //        new ImprimeMatriz((maty2);        
 //        System.out.println("Cr ");
 //        new ImprimeMatriz((matcr2);
 //        System.out.println("Cb ");
 //        new ImprimeMatriz((matcb2);  
         
 //        cvShowImage("ORIGINAL (RGB) ", img);
 //        cvShowImage("HSV", hsvimg);
 //        cvShowImage("ESCALA DE CINZA", cinzaimg);
 //        cvShowImage("val", val);
 //        cvShowImage("hue", hue);
 //        cvShowImage("sat", sat);
 //        cvShowImage("R", r);
 //        cvShowImage("G", g);
 //        cvShowImage("B", b);
 //        cvWaitKey();        
 //        cvSaveImage("Imagens/Resultados/HSV.jpg",hsvimg);
 //        cvSaveImage("Imagens/Resultados/CINZA.jpg",cinzaimg);
         
         
 //        Histograma hr = new Histograma(matr);
 //        tr = new Thread(hr);
 //        tr.start();        
 //        Histograma hg = new Histograma(matg);
 //        tg = new Thread(hg);
 //        tg.start();
 //        Histograma hb = new Histograma(matb);
 //        tb = new Thread(hb);
 //        tb.start();
 //        HistogramaHSV hh = new HistogramaHSV(mathue);
 //        th = new Thread(hh);
 //        th.start();
 //        HistogramaHSV hs = new HistogramaHSV(matsat);
 //        ts = new Thread(hs);
 //        ts.start();
 //        HistogramaHSV hv = new HistogramaHSV(matval);
 //        tv = new Thread(hv);
 //        tv.start();
 
         
 //        
 //        Histograma Hy = new Histograma(maty2);
 //        tY= new Thread(Hy);
 //        tY.start();       
 //        
 //        Histograma hCb = new Histograma(matcb2);
 //        tCb = new Thread(hCb);
 //        tCb.start();
 //        
 //        Histograma hCr = new Histograma(matcr2);
 //        tCr = new Thread(hCr);
 //        tCr.start();
         
         //Histograma rgb=new Histograma(hr,hg,hb);
         
 
         while( tr.isAlive() || tg.isAlive() || tb.isAlive() || th.isAlive() ||ts.isAlive() || tv.isAlive()|| tcinza.isAlive() || tY.isAlive() || tCb.isAlive() || tCr.isAlive());
         
 //        System.out.println("R");
 //        hr.mostra_abs();
 //        hr.mostra_norm();
 //        System.out.println("G");
 //        hg.mostra_abs();
 //        hg.mostra_norm();
 //        System.out.println("B");
 //        hb.mostra_abs();
 //        hb.mostra_norm();
 //        
 //        System.out.println("H");
 //        hh.mostra_abs();
 //        System.out.println("S");
 //        hs.mostra_abs();
 //        System.out.println("V");
 //        hv.mostra_abs();
 //        
 //        System.out.println("CINZA");
 //        hcinza.mostra_abs();
         
 //        System.out.println("Y");
 //        Hy.mostra_abs();
 //        System.out.println("Cb");
 //        hCb.mostra_abs();
 //        System.out.println("Cr");
 //        hCr.mostra_abs();      
                      
              
         
         cvReleaseImage(img);
         cvReleaseImage(cinzaimg);
 //        cvReleaseImage(hsvimg);        
 //        cvReleaseImage(r);
 //        cvReleaseImage(g);
 //        cvReleaseImage(b);
 //        cvReleaseImage(hue);
 //        cvReleaseImage(sat);
 //        cvReleaseImage(val);
 //        cvReleaseImage(YCrCbimg);
 //        cvReleaseImage(y);
 //        cvReleaseImage(cb);
 //        cvReleaseImage(cr);
         
         
     
         
     }
  
   public  MatrizPixel (String imagem, boolean matboolean[][]) {
         // TODO code application logic here
         
         int altura,comprimento;
         startThread st = new startThread();
         Thread tr=new Thread(st),tg=new Thread(st),tb=new Thread(st),th=new Thread(st),ts=new Thread(st),tv=new Thread(st),tcinza=new Thread(st),tY=new Thread(st),tCb=new Thread(st),tCr=new Thread(st);
         
         
        
         
         IplImage img = cvLoadImage(imagem);
         
         // conversao para hsv e escala de cinza
         IplImage hsvimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);
         IplImage cinzaimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,1);        
         IplImage YCrCbimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);        
         IplImage YUVimg = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);        
         cvCvtColor(img,hsvimg,CV_BGR2HSV);
         cvCvtColor(img,cinzaimg,CV_BGR2GRAY);
         cvCvtColor(img,YCrCbimg,CV_BGR2YCrCb);
         cvCvtColor(img,YUVimg,CV_BGR2YUV);
         
         // separaÃ§Ã£o dos canais R G B         
         IplImage r = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
         IplImage g = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
         IplImage b = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
         cvSplit( img, b, g, r, null );       
 
         // separaÃ§Ã£o dos canais H S V
         IplImage hue = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
         IplImage sat = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
         IplImage val = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);        
         cvSplit( hsvimg, hue, sat, val, null );
         
         // separaÃ§Ã£o dos canais Y Cr Cb
         IplImage y = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
         IplImage cr = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
         IplImage cb = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);        
         cvSplit( YCrCbimg,y, cr, cb, null );
         
         // pegar valor de cada pixel R G B
 //        CvScalar s;
 //        Double matb[][] = new Double[b.height()][b.width()];
 //        Double matg[][] = new Double[g.height()][g.width()];
 //        Double matr[][] = new Double[r.height()][r.width()];
 //        for(int i=0;i<img.height();i++)
 //            for(int j=0;j<img.width();j++){
 //                if(matboolean[i][j]){
 //                    s=cvGet2D(img,i,j);
 //                    matb[i][j]=s.val(0);
 //                    matg[i][j]=s.val(1);
 //                    matr[i][j]=s.val(2);
 //                }
 //                else{
 //                    matb[i][j]= 1.0;
 //                    matg[i][j]= 1.0;
 //                    matr[i][j]= 1.0;
 //                }
 //            }
 //        System.out.println("R ");
 //        new ImprimeMatriz(matr);        
 //        System.out.println("G ");
 //        new ImprimeMatriz(matg);
 //        System.out.println("B ");
 //        new ImprimeMatriz(matb);
         
         
         // pegar valor de cada pixel H S V
 //        CvScalar s2 = null;
 //        double aux;
 //        Double mathue[][] = new Double[hue.height()][hue.width()];
 //        Double matsat[][] = new Double[sat.height()][sat.width()];
 //        Double matval[][] = new Double[val.height()][val.width()];
 //        for(int i=0;i<hsvimg.height();i++)
 //            for(int j=0;j<hsvimg.width();j++){
 //                if(matboolean[i][j]){
 //                    s2=cvGet2D(hsvimg,i,j);
 //                    mathue[i][j]=s2.val(0)/1.8;
 //                    matsat[i][j]=round(s2.val(1)/2.56,0);
 //                    matval[i][j]=round(s2.val(2)/2.56,0);
 //                }
 //                else{
 //                    mathue[i][j]= 1.0;
 //                    matsat[i][j]= 1.0;
 //                    matval[i][j]= 1.0;
 //                }
 //            }
 //        System.out.println("\nh ");
 //        new ImprimeMatriz((mathue);        
 //        System.out.println("s ");
 //        new ImprimeMatriz((matsat);
 //        System.out.println("v ");
 //        new ImprimeMatriz((matval);
         
         
         // pegar valor de cada pixel escala de cinza
 //        CvScalar s3 = null;
 //        Double matcinza[][] = new Double[cinzaimg.height()][cinzaimg.width()];
 //        
 //        for(int i=0;i<r.height();i++)
 //            for(int j=0;j<r.width();j++){
 //                s3=cvGet2D(cinzaimg,i,j);
 //                matcinza[i][j]=s3.val(0);
 //                
 //            }
 
         
 //        System.out.println("\ncinza ");
 //        new ImprimeMatriz((matcinza);  
         
          // pegar valor de cada pixel Y Cr Cb
 //        CvScalar s4;
 //        Double maty[][] = new Double[y.height()][y.width()];
 //        Double matcr[][] = new Double[cr.height()][cr.width()];
 //        Double matcb[][] = new Double[cb.height()][cb.width()];
 //        for(int i=0;i<YCrCbimg.height();i++)
 //            for(int j=0;j<YCrCbimg.width();j++){
 //                if(matboolean[i][j]){
 //                    s4=cvGet2D(img,i,j);
 //                    maty[i][j]=s4.val(1);
 //                    matcr[i][j]=s4.val(2);
 //                    matcb[i][j]=s4.val(0);
 //                }
 //                else{
 //                    maty[i][j]= 1.0;
 //                    matcr[i][j]= 1.0;
 //                    matcb[i][j]= 1.0;
 //                }
 //            }
 //        System.out.println("Y ");
 //        new ImprimeMatriz((maty);        
 //        System.out.println("Cr ");
 //        new ImprimeMatriz((matcr);
 //        System.out.println("Cb ");
 //        new ImprimeMatriz((matcb);
         
 
 //        Double maty2[][] = new Double[y.height()][y.width()];
 //        Double matcr2[][] = new Double[cr.height()][cr.width()];
 //        Double matcb2[][] = new Double[cb.height()][cb.width()];
 //        Double vet[];
 //        for(int i=0;i<YCrCbimg.height();i++)
 //            for(int j=0;j<YCrCbimg.width();j++){
 //                if(matboolean[i][j]){
 //                    s4=cvGet2D(img,i,j);
 //                    vet = converter_RGB_para_YCbCr(s4.val(2),s4.val(1), s4.val(0));
 //                    maty2[i][j]=vet[0];
 //                    matcr2[i][j]=vet[1];
 //                    matcb2[i][j]=vet[2];
 //                }
 //                else{
 //                    maty2[i][j]= 1.0;
 //                    matcr2[i][j]= 1.0;
 //                    matcb2[i][j]= 1.0;
 //                }
 //                
 //            }
 //        System.out.println("Y ");
 //        new ImprimeMatriz((maty2);        
 //        System.out.println("Cr ");
 //        new ImprimeMatriz((matcr2);
 //        System.out.println("Cb ");
 //        new ImprimeMatriz((matcb2);  
         
 //        cvShowImage("ORIGINAL (RGB) ", img);
 //        cvShowImage("HSV", hsvimg);
 //        cvShowImage("ESCALA DE CINZA", cinzaimg);
 //        cvShowImage("val", val);
 //        cvShowImage("hue", hue);
 //        cvShowImage("sat", sat);
 //        cvShowImage("R", r);
 //        cvShowImage("G", g);
 //        cvShowImage("B", b);
 //        cvWaitKey();        
 //        cvSaveImage("Imagens/Resultados/HSV.jpg",hsvimg);
 //        cvSaveImage("Imagens/Resultados/CINZA.jpg",cinzaimg);
         
         
 //        Histograma hr = new Histograma(matr);
 //        tr = new Thread(hr);
 //        tr.start();        
 //        Histograma hg = new Histograma(matg);
 //        tg = new Thread(hg);
 //        tg.start();
 //        Histograma hb = new Histograma(matb);
 //        tb = new Thread(hb);
 //        tb.start();
 //        HistogramaHSV hh = new HistogramaHSV(mathue);
 //        th = new Thread(hh);
 //        th.start();
 //        HistogramaHSV hs = new HistogramaHSV(matsat);
 //        ts = new Thread(hs);
 //        ts.start();
 //        HistogramaHSV hv = new HistogramaHSV(matval);
 //        tv = new Thread(hv);
 //        tv.start();
         
 //        Histograma hcinza = new Histograma(matcinza);
 //        tcinza = new Thread(hcinza);
 //        tcinza.start();
         
 
                 
 //        
 //        Histograma Hy = new Histograma(maty2);
 //        tY= new Thread(Hy);
 //        tY.start();       
 //        
 //        Histograma hCb = new Histograma(matcb2);
 //        tCb = new Thread(hCb);
 //        tCb.start();
 //        
 //        Histograma hCr = new Histograma(matcr2);
 //        tCr = new Thread(hCr);
 //        tCr.start();
         
         //Histograma rgb=new Histograma(hr,hg,hb);
         
         //  OBS: TODAS AS FUNCOES DE POS PROCESSAMENTO QUE DEPENDAM DOS HISTOGRAMAS DEVEM SER CHAMADAS ABAIXO DO WHILE 
 
         while( tr.isAlive() || tg.isAlive() || tb.isAlive() || th.isAlive() ||ts.isAlive() || tv.isAlive()|| tcinza.isAlive() || tY.isAlive() || tCb.isAlive() || tCr.isAlive());
         
 //        Equalizacao eqcinza = new Equalizacao(cinzaimg);
 //        cvShowImage("Imagem Equalizada", eqcinza.getImagemEqualizada());        
 //        cvWaitKey();
         
 //        Mediana mediana = new Mediana(40,cinzaimg);
 //        cvShowImage("Imagem Filtro da mediana", mediana.getImagemMediana());        
 //        cvWaitKey();
           
 //          Sobel sobel = new Sobel(cinzaimg,1,1,7);
 //          cvShowImage("Imagem Filtro Sobel", sobel.getImagemSobel()); 
 //          cvWaitKey();
           
         
         // EROCAO   DILATAVCAO   ABERTURA   FECHAMENTO   CONTORNO
         
         
         IplImage im_bw = cvCreateImage(cvGetSize(cinzaimg),IPL_DEPTH_8U,1);      
         cvThreshold(cinzaimg, im_bw, 128, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
         
         //IplImage im_dilatacao = cvCreateImage(cvGetSize(cinzaimg),IPL_DEPTH_8U,1);
         //cvDilate(im_bw, im_dilatacao, null, 1);
         
         //IplImage im_erosao = cvCreateImage(cvGetSize(cinzaimg),IPL_DEPTH_8U,1);
         //cvErode(im_bw, im_erosao, null,2);
         
         //cvShowImage("Imagem Dilatacao", im_dilatacao); 
         cvWaitKey();
         
         
         
         
         
 //        System.out.println("R");
 //        hr.mostra_abs();
 //        hr.mostra_norm();
 //        System.out.println("G");
 //        hg.mostra_abs();
 //        hg.mostra_norm();
 //        System.out.println("B");
 //        hb.mostra_abs();
 //        hb.mostra_norm();
 ////        
 //        System.out.println("H");
 //        hh.mostra_abs();
 //        System.out.println("S");
 //        hs.mostra_abs();
 //        System.out.println("V");
 //        hv.mostra_abs();
 //        
 //        System.out.println("CINZA");
 //        hcinza.mostra_abs();
         
 //        System.out.println("Y");
 //        Hy.mostra_abs();
 //        System.out.println("Cb");
 //        hCb.mostra_abs();
 //        System.out.println("Cr");
 //        hCr.mostra_abs();      
                      
              
         
         cvReleaseImage(img);
         cvReleaseImage(hsvimg);
         cvReleaseImage(cinzaimg);
         cvReleaseImage(r);
         cvReleaseImage(g);
         cvReleaseImage(b);
         cvReleaseImage(hue);
         cvReleaseImage(sat);
         cvReleaseImage(val);
         cvReleaseImage(YCrCbimg);
         cvReleaseImage(y);
         cvReleaseImage(cb);
         cvReleaseImage(cr);
         
         
     
         
     }
  
     
     private static double round(double value, int places) {
         if (places < 0) throw new IllegalArgumentException();
 
         BigDecimal bd = new BigDecimal(value);
         bd = bd.setScale(places, RoundingMode.HALF_UP);
         return bd.doubleValue();
 }
     
     private static Double[] converter_RGB_para_YCbCr(Double R, Double G, Double B){
     Double YCbCr[] = new Double[3];
 
     double delta = 128.0; //Constante necessaria para o calculo da conversÃ£o de cor
     double Y  = (0.299 * R + 0.587 * G + 0.114 * B);
     double Cb = ((B - Y) * 0.564 + delta);
     double Cr = ((R - Y) * 0.713 + delta);
 
     YCbCr[0] =  Y;
     YCbCr[1] =  Cb;
     YCbCr[2] =  Cr;
     return YCbCr;
 }
     
     
     
 
 }

