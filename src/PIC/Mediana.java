/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PIC;

import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvGet2D;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvSet2D;
import static com.googlecode.javacv.cpp.opencv_highgui.cvShowImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvWaitKey;

/**
 *
 * @author Gianfranco
 * @param tam = tamanho da mascara, numero de pixels acima, abaixo, a direita e a esquerda do pixel central
 */
public class Mediana {
    
    private Integer matriz[][],matmed[][],valores[];
    private Integer tam,nvalores,indicemedio;
    private IplImage img,resultado;

    public Mediana(Integer tam, IplImage img) {
        this.tam = tam;
        this.img = img;


        nvalores=(1+2*tam)*(1+2*tam);
        indicemedio=nvalores/2;
        valores = new Integer[nvalores];
        for (int i = 0; i < nvalores; i++) valores[i]=0;
        
        Matriz mat = new Matriz(img);
        matriz = mat.getMat();
        
        matmed = new Integer[img.height()][img.width()];
        
        int x=0,y=0;
        for (int i = 0; i < img.height(); i++) 
            for (int j = 0; j < img.width(); j++){                 
                matmed[i][j]=0;
                this.SalvaVizinhos(i,j);
            }
        this.criaImagem();
            
        
    }   
       
// esta confuso as coordenadas (x,y) e (i,j) mas ta funcionando
    private void SalvaVizinhos(int y,int x) {

        Integer valormedio = 0;
        int o=0;
        int inicioy = x - tam, iniciox = y - tam;
        if (iniciox < 0) {
            iniciox = 0;
        }
        if (inicioy < 0) {
            inicioy = 0;
        }

            for (int i = iniciox; i <= iniciox + tam * 2; i++) {
                for (int j = inicioy; j <= inicioy + tam * 2; j++) {
                    if (i >= matriz.length || j >= matriz[0].length); else {
                        valores[o]= matriz[i][j];
                        o++;
                    }
                }
            }            
        
        QuickSort qs = new QuickSort();
        qs.sort(valores);
        matmed[y][x]=valores[indicemedio];

    }
    
    private void criaImagem() {
        resultado = cvCreateImage(cvGetSize(img), img.depth(), img.nChannels());
        CvScalar pixel = new CvScalar();
        for (int j = 0; j < resultado.height(); j++) {
            for (int i = 0; i < resultado.width(); i++) {
                if (!matmed[j][i].equals(null)) {
                    pixel = cvGet2D(img, j, i);
                    pixel.setVal(0, matmed[j][i]);
                    cvSet2D(resultado, j, i, pixel);
                }
            }
        }
    }

    public IplImage getImagemMediana() {
        return resultado;
    }
    
    
    
    
}