
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvGet2D;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvSet2D;
import static com.googlecode.javacv.cpp.opencv_highgui.cvShowImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvWaitKey;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_MEDIAN;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvSmooth;

 /**
  *
  * @author Gianfranco
  */
 public class LBP {
 
     private Integer[][] m = null;
 
     public LBP(Integer[][] matriz) {
 
         m = new Integer[matriz.length][matriz[0].length];
 
         for (int i = 0; i < matriz.length; i++) {
             for (int j = 0; j < matriz[0].length; j++) {
                 m[i][j] = this.Vizinhos(matriz, i, j, 1);
             }
         }
 
     }
 
     private Integer Vizinhos(Integer[][] mat, int x, int y, int tam) {
 
         Integer valor = null;
         int div = 0;
         String binario = "";
         int iniciox = x-tam, inicioy = y-tam;
         if (iniciox < 0) {
             iniciox = 0;
         }
         if (inicioy < 0) {
             inicioy = 0;
         }
         for (int i = iniciox; i <= iniciox + tam * 2; i++) {
             for (int j = inicioy; j <= inicioy + tam * 2; j++) {
                 if (i >= mat.length || j >= mat[0].length); else {
                     if (i == x && j == y); else {
                         if (mat[i][j] >= mat[x][y]) {
                             binario = binario.concat("0");
                         } else {
                             binario = binario.concat("1");
                         }
 
                     }
                 }
             }
         }
         
         return Integer.parseInt(binario, 2);
 
     }
     
     public static void mostraLBp(IplImage img, LBP lbp ){
         
        CvScalar pixel = new CvScalar();
        Integer mat[][] = lbp.getM();
        IplImage resultado = cvCreateImage(cvGetSize(img), img.depth(), img.nChannels());       
        for(int i=0;i<resultado.width();i++)
            for(int j=0;j<resultado.height();j++)
                if(mat[j][i]!=null){
                    pixel=cvGet2D(img,j,i);
                    pixel.setVal(0,mat[j][i]);
                    pixel.setVal(1,mat[j][i]);
                    pixel.setVal(2,mat[j][i]);                    
                                  
                    cvSet2D(resultado,j,i, pixel);
                }
        
        cvShowImage("LBP", resultado);
        
        cvWaitKey();
     }
 
     public Integer[][] getM() {
         return m;
     }
 
 }

