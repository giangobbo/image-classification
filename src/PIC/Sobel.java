/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PIC;

import static com.googlecode.javacv.cpp.opencv_core.CV_8U;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvSobel;

/**
 *
 * @author Gianfranco
 */
public class Sobel {
    
    private IplImage img,resultado;
    private Integer xorder,yorder,aperture_size;

    public Sobel(IplImage img, Integer xorder, Integer yorder, Integer aperture_size) {
        this.img = img;
        this.xorder = xorder;
        this.yorder = yorder;
        this.aperture_size=aperture_size;
        
        resultado = cvCreateImage(cvGetSize(img), img.depth(), img.nChannels());
        
        filtro();
    }
    
    private void filtro(){
        cvSobel(img, resultado, xorder, yorder, aperture_size);                
    }

    public IplImage getImagemSobel() {
        return resultado;
    }    
        
}
