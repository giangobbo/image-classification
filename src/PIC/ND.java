
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import java.util.Comparator;
 
 /**
  *
  * @author Gianfranco
  */
 public class ND {
     
     private String nome;
     private Double distancia;
 
     public ND(String nome, Double distancia) {
         this.nome = nome;
         this.distancia = distancia;
     }
 
     public String getNome() {
         return nome;
     }
 
     public void setNome(String nome) {
         this.nome = nome;
     }
 
     public Double getDistancia() {
         return distancia;
     }
 
     public void setDistancia(Double distancia) {
         this.distancia = distancia;
     }
 
     @Override
     public String toString() {
         return "imagem:" + nome + ", diferenca=" + distancia;
     }   
     
     
     
 }
 
 

