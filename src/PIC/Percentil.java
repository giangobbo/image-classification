
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 /**
  *
  * @author Gianfranco
  */
 public class Percentil {
 
     private Integer[] percentil_abs;
     private Double[] percentil;
     private Double intervalo;
     private Double sum;
 
     public Percentil(int n, Double[] histograma) {
         
         Double soma=0.0;
         int z=0;
         
         //arredondamento da soma do histograma para 1
         for (int j = 0; j < histograma.length; j++)
             if(histograma[histograma.length-j-1]!=0.0)
                 z=j;
         for (int j= 0; j < z; j++)
             soma+=histograma[j];
         histograma[z]=1-soma;
         
 
         percentil_abs = new Integer[n+1];
         percentil = new Double[n];
         sum=0.0; 
         intervalo = 1 / (double) n;
         int i = 0;
         boolean aux=false;
         for (int j = 0; j < histograma.length && i<percentil_abs.length; j++) {
             sum += histograma[j];
             if(i==0 && sum!=0){
                 percentil_abs[i]=j;
                 i++;
                 aux=true;
             }
             else if (aux== true && sum >= intervalo * i) {
                 percentil_abs[i] = j;
 //                    if ((i+1)<percentil_abs.length && sum >= intervalo * (i + 2)){
 //                        percentil_abs[i+1] = j;       
 //                        i++;
 //                    }
                 percentil[i-1]=(percentil_abs[i]-percentil_abs[i-1])/(double)255;
                 i++;
             }            
         }
         if(i<(percentil_abs.length-1))
             for (int j = i; j < percentil_abs.length; j++){
                 percentil_abs[j]=255;
                 percentil[j-1]=(percentil_abs[j]-percentil_abs[j-1])/(double)255;
             }
         
         
 
     }
     
     
 
     public Integer[] getPercentil_abs() {
         return percentil_abs;
     }
 
     public Double[] getPercentil() {
         return percentil;
     }
 
     
     public String string_abs() {
         String ret = "";
         for (int i = 0; i < percentil_abs.length; i++) {
             try {
                 ret = ret.concat(" ").concat(Integer.toString(percentil_abs[i]));
             } catch (NullPointerException n) {
                 ret = ret.concat(" N/A");
             }
         }
         return ret;
     }
     
     public String string_norm() {
         String ret = "";
         for (int i = 0; i < percentil.length; i++) {
             try {
                 ret = ret.concat(" ").concat(Double.toString(percentil[i]));
             } catch (NullPointerException n) {
                 ret = ret.concat(" N/A");
             }
         }
         return ret;
     }
 
 }

