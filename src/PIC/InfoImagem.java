
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 /**
  *
  * @author Gianfranco
  */
 
 /*
 histogramas[npartes][4][3][ ]
                      |  |  |
                      |  |  |--array de dados
                      |  |
                      |  |--indice 0 :histograma do canal
                      |  |-- indice 1 :histograma do percentil
                      |  |-- indice 2 :histograma do LBP
                      |
                      |--indice 0: canal R
                      |--indice 1: canal G
                      |--indice 2: canal B                
                      |--indice 3: canal Cinza                
 
 */
 
 
 public class InfoImagem {
     
     private Double[][][][] histrogramas;
     private Integer npartes,npercentil;
     private String nome;
 
     public InfoImagem(Integer npartes, Integer npercentil, String nome) {        
         this.nome = nome;        
         histrogramas = new Double[npartes][4][3][256];
     }
 
 
     public void setHistrogramas(int parte,int rgbc, Double[] h, Double[] percentil, Double[] lbp) {        
         
         histrogramas[parte][rgbc][0]=h;
         histrogramas[parte][rgbc][1]=percentil;
         histrogramas[parte][rgbc][2]=lbp;
         
 
     }
     
     public static Double[] stringToDouble(String s){
         String[] aux = s.split(" ");
         Double[] ret = new Double[aux.length];
         
         for(int i=0;i<aux.length;i++){
             ret[i] = Double.parseDouble(aux[i]);
         }
         
         return ret;
     }
 
     public Double[][][][] getHistrogramas() {
         return histrogramas;
     }
        
     
     public Double[] getHistogramaR(int parte){
         return histrogramas[parte][0][0];
     }
     
     public Double[] getHistogramaG(int parte){
         return histrogramas[parte][1][0];
     }
     
     public Double[] getHistogramaB(int parte){
         return histrogramas[parte][2][0];
     }
     
     public static Double diferenca(Double[][][][] a, Double[][][][] b, Integer op){
         Double x = 0.0;
         
         if(op==0){
         for(int i = 0; i < a.length; i++)
             for(int j = 0; j < a[0].length; j++)
                 for(int k = 0; k < a[0][0].length; k++)
                     for(int l = 0; l < a[0][0][k].length; l++)
                         if(a[i][j][k][l]==null || b[i][j][k][l]==null) x+=0;
                         else x=x+Math.sqrt((a[i][j][k][l]-b[i][j][k][l])*(a[i][j][k][l]-b[i][j][k][l]));
//            for(int i = 0; i < a.length; i++)
//                for(int j = 0; j < a[0].length; j++)
//                    for(int k = 0; k < a[0][0].length; k++){
//                        if(k==0)
//                            for(int l=0;l<10;l++){
////                                System.out.println(i);
////                                System.out.println(j);
////                                System.out.println(k);
////                                System.out.println(l);
//                               if(a[i][j][k][l]==null || b[i][j][k][l]==null) x+=0;
//                               else x=x+Math.sqrt((a[i][j][k][l]-b[i][j][k][l])*(a[i][j][k][l]-b[i][j][k][l]));
//                            }
//                        if(k==1)
//                            for(int l=0;l<a[0][0][k].length;l++)
//                                if(a[i][j][k][l]==null || b[i][j][k][l]==null) x+=0;
//                                else x=x+Math.sqrt((a[i][j][k][l]-b[i][j][k][l])*(a[i][j][k][l]-b[i][j][k][l]));
//                        if(k==2)
//                            for(int l=0;l<256;l++){
//                                //System.out.println(a[0][0][k].length);
//                                System.out.println(a[i][j][k][l]);
//                                if(a[i][j][k][l]==null || b[i][j][k][l]==null) x+=0;
//                                else x=x+Math.sqrt((a[i][j][k][l]-b[i][j][k][l])*(a[i][j][k][l]-b[i][j][k][l]));
//                            }
//                    }
                        
             
         }
         if(op==1)
         for(int i = 0; i < a.length; i++)
             for(int j = 0; j < a[0].length; j++)
                     for(int l = 0; l < a[0][0][1].length; l++)
                         if(a[i][j][1][l]==null || b[i][j][1][l]==null) x+=0;
                         else x=x+Math.sqrt((a[i][j][1][l]-b[i][j][1][l])*(a[i][j][1][l]-b[i][j][1][l]));
         if(op==2)
         for(int i = 0; i < a.length; i++)
             for(int j = 0; j < a[0].length; j++)                 
                     for(int l = 0; l < a[0][0][2].length; l++)
                         if(a[i][j][2][l]==null || b[i][j][2][l]==null) x+=0;
                         else x=x+Math.sqrt((a[i][j][2][l]-b[i][j][2][l])*(a[i][j][2][l]-b[i][j][2][l]));
                         
        
         return x;
     }
     
     
     
     
     
   
   
     
 }

