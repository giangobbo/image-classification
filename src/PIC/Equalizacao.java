/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PIC;

import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvGet2D;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvSet2D;

/**
 *
 * @author Gianfranco
 */
public class Equalizacao {

    private Histograma h, histograma_equalzado;
    private Integer novo_nivel_,limite_superior = 0, novoNivel[],heq[];
    private Double sk[],sklm[];
    private Integer matriz[][];
    private IplImage img,resultado;
    private Thread t;

        public Equalizacao(IplImage img, Histograma h, Double[][] matriz) {

        this.img = img;

        int tamanho = h.getHistograma_abs().length;
        histograma_equalzado = new Histograma(tamanho);
        sk = new Double[tamanho];
        sklm = new Double[tamanho];
        novoNivel = new Integer[tamanho];
        heq = new Integer[tamanho];

        int x = tamanho - 1;
        while (h.getHistograma_abs()[x].equals(0)) {
            x--;
        }
        limite_superior = x;

        for (int i = 0; i < sk.length; i++) {
            sk[i] = 0.0;
        }

        equalizador();
        criaImagem();
    }

    public Equalizacao(IplImage img) {

        this.img=img;
        Matriz mat = new Matriz(img);
        matriz = mat.getMat();

        h = new Histograma(matriz);
        t = new Thread((Runnable) h);
        t.start();
        while(t.isAlive());        

        int tamanho = h.getHistograma_abs().length;
        histograma_equalzado = new Histograma(tamanho);
        sk = new Double[tamanho];
        sklm = new Double[tamanho];
        novoNivel = new Integer[tamanho];
        heq = new Integer[tamanho];
        

        int x = tamanho - 1;
        while (h.getHistograma_abs()[x].equals(0)) {
            x--;
        }
        limite_superior = x;

        for (int i = 0; i < sk.length; i++) {
            sk[i] = 0.0;
        }

        equalizador();
        criaImagem();
    }

    private void equalizador() {

        for (int i = 1; i < sk.length; i++) {
            sk[i] = h.getHistograma_norm()[i] + sk[i - 1];
        }

        for (int j = 0; j < sk.length; j++) {
            sklm[j] = sk[j] * limite_superior;
        }

        for (int j = 0; j < sk.length; j++) {
            novoNivel[j] = (int) Math.round(sklm[j]);
        }

    }

    public Histograma getHistogramaEqualizado() {
        
        for (int j = 0; j < heq.length; j++) {
            heq[j]=0;
        }
        for (int j = 0; j < heq.length; j++) {
            heq[novoNivel[j]]++;
        }
        return new Histograma(heq);
        
    }

    public Integer[][] getMatriz() {
        return matriz;
    }

    public Histograma getHistogramaImagem() {
        return h;
    }
      
       

    private void criaImagem() {

        resultado = cvCreateImage(cvGetSize(img), img.depth(), img.nChannels());
        Double mateq[][] = new Double[img.height()][img.width()];

        for (int i = 0; i < img.height(); i++) {
            for (int j = 0; j < img.width(); j++) {
                mateq[i][j] = (double) novoNivel[matriz[i][j].intValue()];
            }
        }

        CvScalar pixel = new CvScalar();
        for (int i = 0; i < resultado.width(); i++) {
            for (int j = 0; j < resultado.height(); j++) {
                if (!mateq[j][i].equals(null)) {
                    pixel = cvGet2D(img, j, i);
                    pixel.setVal(0, mateq[j][i]);
                    cvSet2D(resultado, j, i, pixel);
                }
            }
        }

    }

    public IplImage getImagemEqualizada() {
        return resultado;
    }
    
}
