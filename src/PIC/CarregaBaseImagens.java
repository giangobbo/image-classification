
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import static com.googlecode.javacv.cpp.opencv_core.CV_8UC1;
 import com.googlecode.javacv.cpp.opencv_core.CvRect;
 import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
 import com.googlecode.javacv.cpp.opencv_core.IplImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvCopy;
 import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
 import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvSetImageROI;
 import static com.googlecode.javacv.cpp.opencv_core.cvSplit;
 import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
 import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
 import java.io.File;
 
 /**
  *
  * @author Gianfranco
  */
 
 /* CABEÃ‡ALHO
      c (numero de imagens) (numero de partes de cada imagem) (numero de percentis) (hÃ¡ histograma canal R) (hÃ¡ histograma canal G) (hÃ¡ histograma canal B) (hÃ¡ histograma canal C)  (hÃ¡ histograma LBP)
 
 */
 public class CarregaBaseImagens {
 
     private File folder;
     private File[] listOfFiles;
     private IplImage imagem;
     private TXT txt;
     private Histograma h, hlbp;
     private Integer[][] histograma_abs_rgb_partes;
     private Double[][] histograma_norm_rgb_partes;
     private Double[][] lbp;
     private Matriz m;
     
     
 
     public CarregaBaseImagens(Integer npercentil,String diretorio, Integer n) {
 
         Integer npartes = n * n;        
         LBP l = null;
         long startTime = System.currentTimeMillis();
         Integer aux;
         folder = new File(diretorio);
         listOfFiles = folder.listFiles();
         txt = new TXT("1000Imagens-RGBC-P-LBP.txt");
         String s = "",cabecalho="-c ";
         IplImage[] imagens = null;
         
         cabecalho=cabecalho.concat(listOfFiles.length+" "+npartes+" "+npercentil+" 1 1 1 1 1");
         cabecalho=cabecalho.concat(" "+diretorio);
         txt.escreve(cabecalho);
         
         for (File listOfFile : listOfFiles) {
             imagem = cvLoadImage(listOfFile.getAbsolutePath());
             imagens = this.DivideImagem(imagem, n);
             h = new Histograma(imagens);
             IplImage r, g, b, c;
             Matriz m;
             txt.escreve("-a " + listOfFile.getName());
             Percentil p = null;
             for (int y = 0; y < npartes; y++) {
                 r = cvCreateImage(cvGetSize(imagens[y]), IPL_DEPTH_8U, CV_8UC1);
                 g = cvCreateImage(cvGetSize(imagens[y]), IPL_DEPTH_8U, CV_8UC1);
                 b = cvCreateImage(cvGetSize(imagens[y]), IPL_DEPTH_8U, CV_8UC1);
                 c = cvCreateImage(cvGetSize(imagens[y]), IPL_DEPTH_8U, CV_8UC1);
                 cvCvtColor(imagens[y], c, CV_BGR2GRAY);
                 cvSplit(imagens[y], b, g, r, null);
                 txt.escreve("PARTE " + (y + 1));
                 p = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][0]);
                 hlbp = new Histograma(new LBP(new Matriz(r).getMat()).getM());
                 txt.escreve("R" + Histograma.string_norm(h.getHistograma_norm_rgb_partes()[y][0]) + "\nPERCENTIL" + p.string_norm() + "\nLBP" + Histograma.string_norm(hlbp.getHistograma_norm()));
                 p = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][1]);
                 hlbp = new Histograma(new LBP(new Matriz(g).getMat()).getM());
                 txt.escreve("G" + Histograma.string_norm(h.getHistograma_norm_rgb_partes()[y][1]) + "\nPERCENTIL" + p.string_norm() + "\nLBP" + Histograma.string_norm(hlbp.getHistograma_norm()));
                 p = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][2]);
                 hlbp = new Histograma(new LBP(new Matriz(b).getMat()).getM());
                 txt.escreve("B" + Histograma.string_norm(h.getHistograma_norm_rgb_partes()[y][2]) + "\nPERCENTIL" + p.string_norm() + "\nLBP" + Histograma.string_norm(hlbp.getHistograma_norm()));
                 p = new Percentil(npercentil, h.getHistograma_norm_rgb_partes()[y][3]);
                 hlbp = new Histograma(new LBP(new Matriz(c).getMat()).getM());
                 txt.escreve("CINZA" + Histograma.string_norm(h.getHistograma_norm_rgb_partes()[y][3]) + "\nPERCENTIL" + p.string_norm() + "\nLBP" + Histograma.string_norm(hlbp.getHistograma_norm()));
                 p = null;
                 r = null;
                 g = null;
                 b = null;
                 c = null;
 
             }
             cvReleaseImage(imagem);
             h = null;
             m = null;
             imagens = null;
         }
 
         long stopTime = System.currentTimeMillis();
 
         txt.escreve("-f "+"QTD IMAGENS  " + listOfFiles.length + " NUMERO DE PARTES POR IMAGEM  " + npartes + " HISTOGRAMA  RGB + CINZA + "+npercentil+" PERCENTIS + LBP\t TEMPO DE EXECUCAO  " + Long.toString(stopTime-startTime) + " ms ");
         txt.fim();
     }
 
     public CarregaBaseImagens(Integer npercentil, String diretorio, Integer n, Integer op) {
 
         long startTime = System.currentTimeMillis();
         Integer npartes = n * n;
 
         Histograma[] hist = null;
         Histograma hlbp = null;
         IplImage[] partes = null;
         folder = new File(diretorio);
         listOfFiles = folder.listFiles();
         txt = new TXT("resultado.txt");
         int aux = 0;
         String s = "",cabecalho="-c";
         IplImage img;
         cabecalho=cabecalho.concat(" "+listOfFiles.length+" "+npartes+" "+npercentil);
         switch (op) {
             case 1: 
                 cabecalho=cabecalho.concat(" 1 0 0 0 1");
                 break;
             case 2: 
                 cabecalho=cabecalho.concat(" 0 1 0 0 1");
                 break;
             case 3: 
                 cabecalho=cabecalho.concat(" 0 0 1 0 1");
                 break;
             case 4: 
                 cabecalho=cabecalho.concat(" 0 0 0 1 1");
                 break;
             default: 
                 cabecalho=cabecalho.concat(" 0 0 0 1 1");
                 break;
         }
         cabecalho=cabecalho.concat(" "+diretorio);
         txt.escreve(cabecalho);
         for (File listOfFile : listOfFiles) {
             hist = new Histograma[npartes];
             partes = new IplImage[npartes];
             imagem = cvLoadImage(listOfFile.getAbsolutePath());
             switch (op) {
                 case 1: //R
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
                     cvSplit(imagem, null, null, img, null);
                     s = "R";
                     break;
                 case 2: //G
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
                     cvSplit(imagem, null, img, null, null);
                     s = "G";
                     break;
                 case 3: //B
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
                     cvSplit(imagem, img, null, null, null);
                     s = "B";
                     break;
                 case 4: // ESCALA DE CINZA
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, 1);
                     cvCvtColor(imagem, img, CV_BGR2GRAY);
                     s = "CINZA";
                     break;
                 default: // ESCALA DE CINZA
                     img = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, 1);
                     cvCvtColor(imagem, img, CV_BGR2GRAY);
                     s = "CINZA";
                     break;
             }
             partes = DivideImagem(img, n);
             txt.escreve("-a " + listOfFile.getName());
             Percentil p = null;
             for (int d = 0; d < npartes; d++) {
                 Matriz m = new Matriz(partes[d]);
                 hist[d] = new Histograma(m.getMat());
                 p = new Percentil(npercentil, hist[d].getHistograma_norm());
                 hlbp = new Histograma(new LBP(m.getMat()).getM());
                 txt.escreve("PARTE " + (d + 1));
                 txt.escreve(s + "" + Histograma.string_norm(hist[d].getHistograma_norm()) + "\nPERCENTIS" + p.string_norm() + "\nLBP" + Histograma.string_norm(hlbp.getHistograma_norm()));
                 m = null;
                 p=null;
                 
                 
             }
             cvReleaseImage(imagem);
             cvReleaseImage(img);
             hist = null;
         }
 
         long stopTime = System.currentTimeMillis();
 
         txt.escreve("-f "+"QTD IMAGENS  " + listOfFiles.length + " HISTOGRAMA " + s + " "+npercentil+" PERCENTIS  "+" LBP "+"\t TEMPO DE EXECUCAO  " + Long.toString(stopTime-startTime) + " ms");
         txt.fim();
 
     }
 
     public static IplImage[] DivideImagem(IplImage img, int n) {
 
         Integer npartes = n * n;
         Integer[] cx, cy;
         cx = new Integer[n + 1];
         cy = new Integer[n + 1];
         Integer auxx = img.width() / n;
         Integer auxy = img.height() / n;
         IplImage[] imgs = new IplImage[npartes];
         IplImage copia = img;
         CvRect[] r = new CvRect[npartes];
         int xi, yi, xf, yf;
 
         for (int i = 0; i <= n; i++) {
             if (i == 0) {
                 cx[i] = 0;
                 cy[i] = 0;
             } else {
                 cx[i] = auxx * i;
                 cy[i] = auxy * i;
             }
         }
         int f = 1, g = 1, q = 0, w = 0, v = 0;
         while (g <= n) {
             while (f <= n) {
                 q = f-1;
                 w = g-1;
                 r[v] = new CvRect();
                 r[v].x(cx[q]);
                 r[v].y(cy[w]);
                 r[v].width(cx[f]-cx[q]);
                 r[v].height(cy[g]-cy[w]);
                 cvSetImageROI(img, r[v]);
                 imgs[v] = cvCreateImage(cvGetSize(img), img.depth(), img.nChannels());
                 cvCopy(img, imgs[v]);
                 img = copia;
                 v++;
                 f++;
             }
             g++;
             f = 1;
 
         }
 
         return imgs;
     }
 
     public void mostra_nomes() {
 
         for (File listOfFile : listOfFiles) {
             if (listOfFile.isFile()) {
                 System.out.println("Arquivo " + listOfFile.getName());
             } else if (listOfFile.isDirectory()) {
                 System.out.println("Directorio " + listOfFile.getName());
             }
         }
     }
 
     public IplImage getImagens() {
         return imagem;
     }
 
 }

