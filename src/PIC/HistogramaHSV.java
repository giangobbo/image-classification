/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PIC;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author Gianfranco
 */
public class HistogramaHSV implements Runnable {
    
    private Integer histograma_abs[];
    private Double histograma_norm[];
    Double matriz[][]=null;
    
    public HistogramaHSV(Double matriz[][]){
        
        histograma_abs = new Integer[100];        
        histograma_norm = new Double[100]; 
        this.matriz=matriz;       
        
        
    }
    
    public HistogramaHSV( HistogramaHSV a, HistogramaHSV b, HistogramaHSV c) {       
        
        histograma_abs = new Integer[100];        
        histograma_norm = new Double[100];
        
        for(int i=0;i<histograma_abs.length;i++){
            histograma_abs[i]=a.getHistograma_abs()[i]+b.histograma_abs[i]+c.getHistograma_abs()[i];
            histograma_norm[i]=a.getHistograma_norm()[i]+b.histograma_abs[i]+c.getHistograma_norm()[i];
        }
        
    }

    public Integer[] getHistograma_abs() {
        return histograma_abs;
    }

    public Double[] getHistograma_norm() {
        return histograma_norm;
    }    
   
    public void mostra_abs(){
        for(int i=0;i<histograma_abs.length;i++)
            System.out.println("i: "+i+" VALOR: "+histograma_abs[i]+" pixels");
    }
    
    public void mostra_norm(){
        for(int i=0;i<histograma_norm.length;i++)
            System.out.printf("i: %d VALOR: %.2f%%\n",i,histograma_norm[i]*100);
    }
    
    private static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
}

    @Override
    public void run() {
        if (matriz != null) {
            for (int i = 0; i < histograma_abs.length; i++) {
                histograma_abs[i] = 0;
            }
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[0].length; j++) {
                    if(!matriz[i][j].equals(-1.0)){
                        int posicao = (int) round(matriz[i][j], 0);
                        if(posicao==100) posicao=99;
                        histograma_abs[posicao]++;
                    }
                }
            }
            int max = matriz.length * matriz[0].length;
            for (int i = 0; i < histograma_norm.length; i++) {
                histograma_norm[i] = 0.0;
            }
            for (int i = 0; i < histograma_norm.length; i++) {
                histograma_norm[i] = (histograma_abs[i] / (double) max);
            }
        }
    }
    
}
