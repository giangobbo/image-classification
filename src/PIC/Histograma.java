
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import static com.googlecode.javacv.cpp.opencv_core.CV_8UC1;
 import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
 import com.googlecode.javacv.cpp.opencv_core.IplImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
 import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
 import static com.googlecode.javacv.cpp.opencv_core.cvSplit;
 import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_HIST_ARRAY;
import com.googlecode.javacv.cpp.opencv_imgproc.CvHistogram;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCalcHist;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCreateHist;
 import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
 import java.math.BigDecimal;
 import java.math.RoundingMode;
 
 /**
  *
  * @author Gianfranco
  */
 public class Histograma {
 
     private Integer histograma_abs[];
     private Integer histograma_abs_rgb[][] = null;
     private Integer histograma_abs_rgb_partes[][][] = null;
     private Integer op;
 
     private Integer[] cx, cy;
     private Double histograma_norm[];
 
     private Double histograma_norm_rgb[][] = null;
     private Double histograma_norm_rgb_partes[][][] = null;
     private Integer matriz[][] = null;
     private Integer matrgb[][][] = null;
     private IplImage[] imagens;
 
     private Double max = 0.0;
     private IplImage r, g, b, c;
 
     public Histograma(Integer matriz[][]) {
 
         histograma_abs = new Integer[256];
         histograma_norm = new Double[256];
         this.matriz = matriz;
         op = 1;
//         calcula();
 
     }
 
     public Histograma(IplImage imagem) {
 
         r = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
         g = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
         b = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, CV_8UC1);
         c = cvCreateImage(cvGetSize(imagem), IPL_DEPTH_8U, 1);
 
         cvSplit(imagem, b, g, r, null);
         cvCvtColor(imagem, c, CV_BGR2GRAY);
         
         
 
         
         matrgb = new Integer[4][imagem.width()][imagem.width()];
         histograma_abs_rgb = new Integer[4][256];
         histograma_norm_rgb = new Double[4][256];
        
 
         matrgb[0] = new Matriz(r).getMat();
         matrgb[1] = new Matriz(g).getMat();
         matrgb[2] = new Matriz(b).getMat();
         matrgb[3] = new Matriz(c).getMat();
 
         for (int o = 0; o < 4; o++) {
             histograma_abs_rgb[o] = new Integer[256];
             histograma_norm_rgb[o] = new Double[256];
         }
 
         op = 2;
         calcula();
     }
 
     public Histograma(IplImage imagem[]) {
 
         int npartes = imagem.length;
         imagens = imagem;
         histograma_abs_rgb_partes = new Integer[npartes][][];
         histograma_norm_rgb_partes = new Double[npartes][][];
 
         for (int i = 0; i < npartes; i++) {
             histograma_abs_rgb_partes[i] = new Integer[4][256];
             histograma_norm_rgb_partes[i] = new Double[4][256];
             for (int o = 0; o < 3; o++) {
                 histograma_abs_rgb_partes[i][o] = new Integer[256];
                 histograma_norm_rgb_partes[i][o] = new Double[256];
             }
         }
 
         op = 3;
         calcula();
 
     }
 
     public Histograma(Integer i) {
 
         histograma_abs = new Integer[i];
         histograma_norm = new Double[i];
 
     }
 
     /**
      *
      * @param a
      */
     public Histograma(Integer a[]) {
 
         histograma_abs = new Integer[a.length];
         histograma_norm = new Double[a.length];
         histograma_abs = a;
         for (int i = 0; i < histograma_norm.length; i++) {
             max += histograma_abs[i];
         }
         for (int i = 0; i < histograma_norm.length; i++) {
             histograma_norm[i] = (histograma_abs[i] / (double) max);
         }
 
     }
 
     public Histograma(Histograma a, Histograma b, Histograma c) {
 
         histograma_abs = new Integer[256];
         histograma_norm = new Double[256];
 
         for (int i = 0; i < histograma_abs.length; i++) {
             histograma_abs[i] = a.getHistograma_abs()[i] + b.getHistograma_abs()[i] + c.getHistograma_abs()[i];
             histograma_norm[i] = a.getHistograma_norm()[i] + b.getHistograma_norm()[i] + c.getHistograma_norm()[i];
         }
 
     }
 
     public Integer[] getHistograma_abs() {
         return histograma_abs;
     }
 
     public Double[] getHistograma_norm() {
         return histograma_norm;
     }
 
     public Integer[][] getMatriz() {
         return matriz;
     }
 
     public Integer[][][] getMatrgb() {
         return matrgb;
     }
 
     public Integer[][] getHistograma_abs_rgb() {
         return histograma_abs_rgb;
     }
 
     public Double[][] getHistograma_norm_rgb() {
         return histograma_norm_rgb;
     }
 
     public Integer[][][] getHistograma_abs_rgb_partes() {
         return histograma_abs_rgb_partes;
     }
 
     public Double[][][] getHistograma_norm_rgb_partes() {
         return histograma_norm_rgb_partes;
     }
 
     public void mostra_abs() {
         for (int i = 0; i < histograma_abs.length; i++) {
             System.out.println(histograma_abs[i]);
         }
     }
 
     public void mostra_norm() {
         for (int i = 0; i < histograma_norm.length; i++) {
             System.out.printf("i: %d VALOR: %.2f%%\n", i, histograma_norm[i] * 100);
         }
     }
 
     public static String string_norm(Double[] h) {
         String ret = "";
         for (int i = 0; i < h.length; i++) {
             ret = ret.concat(" ").concat(Double.toString(h[i]));
         }
         return ret;
     }
 
     public static String string_norm(Integer[] h) {
         String ret = "";
         for (int i = 0; i < h.length; i++) {
             try {
                 ret = ret.concat(" ").concat(Integer.toString(h[i]));
             } catch (NullPointerException n) {
                 ret = ret.concat(" N/A");
             }
         }
         return ret;
     }
 
     private static double round(double value, int places) {
         if (places < 0) {
             throw new IllegalArgumentException();
         }
 
         BigDecimal bd = new BigDecimal(value);
         bd = bd.setScale(places, RoundingMode.HALF_UP);
         return bd.doubleValue();
     }
 
     
 
     private void calcula() {
 
         switch (op) {
             case 1:
                 if (matriz != null) {
                     for (int i = 0; i < histograma_abs.length; i++) {
                         histograma_abs[i] = 0;
                     }
                     for (int i = 0; i < matriz.length; i++) {
                         for (int j = 0; j < matriz[0].length; j++) {
                             if (!matriz[i][j].equals( 1.0)) {
                                 int posicao = (int) round(matriz[i][j], 0);
                                 histograma_abs[posicao]++;
                             }
                         }
                     }
                     int max = matriz.length * matriz[0].length;
                     for (int i = 0; i < histograma_norm.length; i++) {
                         histograma_norm[i] = (histograma_abs[i] / (double) max);
                     }
                 }
                 break;
             case 2:
                 for (int x = 0; x < matrgb.length; x++) {
                     matriz = matrgb[x];
                     if (matriz != null) {
                         for (int i = 0; i < histograma_abs_rgb[x].length; i++) {
                             histograma_abs_rgb[x][i] = 0;
                         }
                         for (int i = 0; i < matriz.length; i++) {
                             for (int j = 0; j < matriz[0].length; j++) {
                                 if (!matriz[i][j].equals( 1.0)) {
                                     int posicao = (int) round(matriz[i][j], 0);
                                     histograma_abs_rgb[x][posicao]++;
                                 }
                             }
                         }
                         int max = matriz.length * matriz[0].length;
                         for (int i = 0; i < histograma_abs_rgb[x].length; i++) {
                             histograma_norm_rgb[x][i] = (histograma_abs_rgb[x][i] / (double) max);
                         }
                     }
                 }
                 break;
             case 3:
                 for (int e = 0; e < imagens.length; e++) {
                     r = cvCreateImage(cvGetSize(imagens[e]), IPL_DEPTH_8U, CV_8UC1);
                     g = cvCreateImage(cvGetSize(imagens[e]), IPL_DEPTH_8U, CV_8UC1);
                     b = cvCreateImage(cvGetSize(imagens[e]), IPL_DEPTH_8U, CV_8UC1);
                     c = cvCreateImage(cvGetSize(imagens[e]), IPL_DEPTH_8U, 1);
 
                     cvSplit(imagens[e], b, g, r, null);
                     cvCvtColor(imagens[e], c, CV_BGR2GRAY);
 
                     matrgb = new Integer[4][imagens[e].width()][imagens[e].width()];
 
                     matrgb[0] = new Matriz(r).getMat();
                     matrgb[1] = new Matriz(g).getMat();
                     matrgb[2] = new Matriz(b).getMat();
                     matrgb[3] = new Matriz(c).getMat();
                     for (int x = 0; x < matrgb.length; x++) {
                         matriz = matrgb[x];
                         if (matriz != null) {
                             for (int i = 0; i < histograma_abs_rgb_partes[e][x].length; i++) {
                                 histograma_abs_rgb_partes[e][x][i] = 0;
                             }
                             for (int i = 0; i < matriz.length; i++) {
                                 for (int j = 0; j < matriz[0].length; j++) {
                                     if (!matriz[i][j].equals( 1.0)) {
                                         int posicao = (int) round(matriz[i][j], 0);
                                         histograma_abs_rgb_partes[e][x][posicao]++;
                                     }
                                 }
                             }
                             int max = matriz.length * matriz[0].length;
                             for (int i = 0; i < histograma_abs_rgb_partes[e][x].length; i++) {
                                 histograma_norm_rgb_partes[e][x][i] = (histograma_abs_rgb_partes[e][x][i] / (double) max);
                             }
                         }
                     }
                     cvReleaseImage(r);
                     cvReleaseImage(b);
                     cvReleaseImage(c);
                     cvReleaseImage(g);
                     matrgb=null;
                 }
                 break;
             default:
                 break;
         }
 
     }
 
 }

