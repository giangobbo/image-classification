/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PIC;

import com.googlecode.javacv.cpp.opencv_core;
import static com.googlecode.javacv.cpp.opencv_core.CV_8UC1;
import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvGet2D;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvSet2D;
import static com.googlecode.javacv.cpp.opencv_core.cvSplit;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvShowImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvWaitKey;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gianfranco
 */

////////////////////        --|LOGICA|--
////////////////////
////////////////////Pega-se cada um dos pixels da imagem e verificasse o valor de R,G e B de seus vizinhos, calculasse
////////////////////a media desses valores e entao comparasse com um valor esperado para o fundo(o que nao é milho) da imgagem,
////////////////////se esse valor estiver proximo entao esse pixel faz parte do fundo da imagem, senao ele faz parte da semente
////////////////////        
////////////////////        --|VARIAVEIS|--
////////////////////fde: fator de aproximação: o limite de quanto o valor da media pode ficar para + ou - do valor esperado para o fundo
////////////////////tam: quantidade de vizinhos que se utiliza para calcular a media em cada direção
////////////////////valorfundor: valor R esperado para o fundo (calculado empiricamente)
////////////////////valorfundog: valor G esperado para o fundo (calculado empiricamente)
////////////////////valorfundob: valor B esperado para o fundo (calculado empiricamente)


public class Segmentador {
    
    private int fde,tam,ix,fx,iy,fy;
    private int valorfundor=0,valorfundog=0,valorfundob=0;
    private boolean  matrm[][],matgm[][],matbm[][],matrgb[][]; 

    public Segmentador(String imagem) {
        
        
        boolean aux;
        
        
        IplImage img = cvLoadImage(imagem);        
        IplImage r = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
        IplImage g = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
        IplImage b = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,CV_8UC1);
        cvSplit( img, b, g, r, null );   
        
        opencv_core.CvScalar s;
        Double matb[][] = new Double[b.height()][b.width()];
        Double matg[][] = new Double[g.height()][g.width()];
        Double matr[][] = new Double[r.height()][r.width()];
        
        
        
        boolean  matbm[][] = new boolean [b.height()][b.width()];
        boolean  matgm[][] = new boolean [g.height()][g.width()];
        boolean  matrm[][] = new boolean [r.height()][r.width()];
        
        for(int i=0;i<img.height();i++)
            for(int j=0;j<img.width();j++){
                matbm[i][j]=true;
                matgm[i][j]=true;
                matrm[i][j]=true;
            }
        
        boolean  matrgb[][] = new boolean [r.height()][r.width()];
        
        for(int i=0;i<img.height();i++)
            for(int j=0;j<img.width();j++){
                s=cvGet2D(img,i,j);
                matb[i][j]=s.val(0);
                matg[i][j]=s.val(1);
                matr[i][j]=s.val(2);
            }
        
        IplImage resultado = cvCreateImage(cvGetSize(img), img.depth(), img.nChannels());       
        
////        ELIMINA O FUNDO
        
        ix=0;iy=0;fx=300;fy=300;tam=1;fde=100;        
        CalculaFundo(matr,matg,matb);        
        
        for(int i=0;i<img.height();i++)
            for(int j=0;j<img.width();j++){
                aux=MediaVizinhos(matb, i, j,valorfundob);
                if(aux==false)matbm[i][j]=aux;
                aux=MediaVizinhos(matg, i, j,valorfundog);
                if(aux==false) matgm[i][j]=aux;
                aux=MediaVizinhos(matr, i, j,valorfundor);
                if(aux==false)matrm[i][j]=aux;
            }
        
        
        
////        ELIMINA A SOMBRA
        
        ix=730;iy=545;fx=755;fy=570;tam=0;fde=100;        
        
       valorfundor=4;valorfundog=25;valorfundob=80;
                
        for(int i=0;i<img.height();i++)
            for(int j=0;j<img.width();j++){
                aux=MediaVizinhos(matb, i, j,valorfundob);
                if(aux==false)matbm[i][j]=aux;
                aux=MediaVizinhos(matg, i, j,valorfundog);
                if(aux==false)matgm[i][j]=aux;
                aux=MediaVizinhos(matr, i, j,valorfundor);
                if(aux==false) matrm[i][j]=aux;
            }
        
        
        for(int i=0;i<img.height();i++)
            for(int j=0;j<img.width();j++){
                if (matbm[i][j]==false && matgm[i][j]==false && matrm[i][j]==false)
                    matrgb[i][j]=false;
                else matrgb[i][j]=true;
                    
            }
        
       SaveObj(matrgb);
       // new ImprimeMatriz(matrgb);
        
        
//////////        BufferedImage res = new BufferedImage(r.width(), r.height(),BufferedImage.TYPE_INT_RGB);
//////////        int preto = 0 | 0 | 0, i = 0,j = 0;
//////////        int branco = 255 | 255 | 255;
//////////        try{
//////////        for(i=0;i<res.getWidth();i++)
//////////            for(j=0;j<res.getHeight();j++)
//////////                if(matrgb[j][i]) res.setRGB(i, j, preto);
//////////                else res.setRGB(i, j, branco);
//////////        } catch(ArrayIndexOutOfBoundsException ex){
//////////            System.out.println("i: "+i+" j: "+j+" img.width(): "+img.width());
//////////            System.out.println(ex.getMessage());
//////////        }
//////////        
//////////        IplImage resultado = IplImage.createFrom(res);
//////////        cvShowImage(imagem, resultado);
//////////                    cvWaitKey();       
        
        
        CvScalar pixel = new CvScalar();
        for(int i=0;i<resultado.width();i++)
            for(int j=0;j<resultado.height();j++)
                if(matrgb[j][i]){
                    pixel=cvGet2D(img,j,i);
                    pixel.setVal(0,matr[j][i]);
                    pixel.setVal(1,matg[j][i]);
                    pixel.setVal(2,matb[j][i]);                    
                                  
                    cvSet2D(resultado,j,i, pixel);
                }
        cvShowImage("semente", resultado);
        
        cvWaitKey();
        
        
                    
        }

    private void SaveObj(boolean[][] mat){
        FileOutputStream saveFile;
        try {
            saveFile = new FileOutputStream("Imagens/obj.seg");
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(mat);
            save.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Segmentador.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Segmentador.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
        
    }
              
 
    
    private void CalculaFundo(Double matr[][],Double matg[][],Double matb[][]){
        
        int div=0;
        valorfundor=0;
        valorfundog=0;
        valorfundob=0;
        for(int i=ix;i<=fx;i++)
            for(int j=iy;j<=fy;j++){
                div++;
                valorfundor+=matr[i][j];
                valorfundog+=matg[i][j];
                valorfundob+=matb[i][j];
            }
        valorfundor=valorfundor/div;
        valorfundog=valorfundog/div;
        valorfundob=valorfundob/div;
        
    }
    
    private boolean  MediaVizinhos(Double mat[][],int x, int y, int valor){
        
        Double valormedio=0.0;
        int div=0;
        int iniciox=x-tam,inicioy=y-tam;
        if(iniciox<0) iniciox=0;
        if(inicioy<0) inicioy=0;        
        for(int i=iniciox;i<=iniciox+tam*2;i++)
            for(int j=inicioy;j<=inicioy+tam*2;j++){
                if(i>=mat.length || j>=mat[0].length);
                else {
                    valormedio+=mat[i][j];
                    div++;
               }
            }
        valormedio=valormedio/div;
        if( valormedio>=valor-fde && valormedio<=valor+fde) return false;        
        else return true;
        
    }
    
}
