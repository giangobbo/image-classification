
 /*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
  */
 package PIC;
 
 import java.io.BufferedWriter;
 import java.io.File;
 import java.io.FileOutputStream;
 import java.io.FileReader;
 import java.io.FileWriter;
 import java.io.IOException;
 import java.io.PrintWriter;
 import java.util.logging.Level;
 import java.util.logging.Logger;
 
 /**
  *
  * @author Gianfranco
  */
 public class TXT {
 
     private FileWriter fw;
     private BufferedWriter bw;
     private PrintWriter out;
 
     public TXT(String arquivo) {
         
         System.out.println(arquivo);
 
         try {
             fw = new FileWriter(arquivo, true);
         } catch (IOException ex) {
             Logger.getLogger(TXT.class.getName()).log(Level.SEVERE, null, ex);
         }
         bw = new BufferedWriter(fw);
         out = new PrintWriter(bw);
 
     }
 
     public void escreve(String texto) {
         out.println(texto);
 
     }
 
     public void fim() {
         out.close();
     }
 }

